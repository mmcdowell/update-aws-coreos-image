#!/usr/bin/python
import boto3
import requests
from datetime import datetime
import base64
import logging
import sys
import argparse

usage = "usage: %prog [options]"
parser = argparse.ArgumentParser(usage)
parser.add_argument('-a', '--autoscalinggroup',dest='autoscaling_group_name')
parser.add_argument('-r', '--role-arn',dest='role_arn')
args = parser.parse_args()

coreosurl = 'https://coreos.com/dist/aws/aws-stable.json'
region = 'us-west-2'
image_type = 'hvm'

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s', stream=sys.stdout)

def main():
    print args.role_arn
    global auto
    if args.role_arn:
        session = assume_role(args.role_arn)
    else:
        session = boto3.session.Session()

    auto = session.client('autoscaling',region_name=region)
    latest_image = get_current_coreos_image(coreosurl, region, image_type)
    launch_configuration = get_launch_configuration(get_launch_configuration_name(args.autoscaling_group_name))

    if compare_lc_image(latest_image,launch_configuration) == True:
        new_lc = copy_lc(launch_configuration,args.autoscaling_group_name,latest_image)
        if new_lc != False:
            assign_lc(new_lc,args.autoscaling_group_name)

def assume_role(role):
    try:
        sts = boto3.client('sts')
        response = sts.assume_role(RoleArn=role,RoleSessionName='sts_session')
        logging.info("[assume_role] IAM Role assumed")
    except Exception as e:
        logging.exception('[assume_role] %s' % e)
        sys.exit(1)
    return boto3.Session(
        aws_access_key_id=response['Credentials']['AccessKeyId'],
        aws_secret_access_key=response['Credentials']['SecretAccessKey'],
        aws_session_token=response['Credentials']['SessionToken'])

def get_current_coreos_image(url,region,image_type):
    try:
        r = requests.get(url)
        if r.status_code >= 200 and r.status_code < 300:
            j = r.json()
            latest_image = j[region][image_type]
            logging.info("[get_current_coreos_image] CoreOS ImageId retrieved sucessfully")

            return latest_image

        else:
            logging.exception('[get_current_coreos_image] Unable to retrieve CoreOS ImageId')
            sys.exit(1)

    except Exception as e:
        logging.exception('[get_current_coreos_image] %s' % e)
        sys.exit(1)

def get_launch_configuration_name(autoscaling_group_name):
    try:
        asg = auto.describe_auto_scaling_groups(AutoScalingGroupNames=[autoscaling_group_name])
        logging.info("[get_launch_configuration_name] AutoScaling Groups retrieved sucessfully")
        if len(asg['AutoScalingGroups']) != 1:
            logging.exception('[get_launch_configuration_name] Did not find exactly 1 launch configuration')

    except Exception as e:
        logging.exception('[get_launch_configuration_name] %s' % e)

    try:
        for i in asg['AutoScalingGroups']:
            if i['AutoScalingGroupName'] == autoscaling_group_name:
                launch_configuration_name = i['LaunchConfigurationName']
                logging.info("[get_launch_configuration_name] Existing Launch Configuration found assigned to %s" % i['AutoScalingGroupName'])

        return launch_configuration_name

    except Exception as e:
        logging.exception('[get_launch_configuration_name] %s' % e)


def get_launch_configuration(launch_configuration_name):
    try:
        lc = auto.describe_launch_configurations(LaunchConfigurationNames=[launch_configuration_name])
        for i in lc['LaunchConfigurations']:
            if i['LaunchConfigurationName'] == launch_configuration_name:
                logging.info("[get_launch_configuration] Found Launch configuration with name: %s" % i['LaunchConfigurationName'])
                return i
    except Exception as e:
        logging.exception('[get_launch_configuration] %s' % e)

def compare_lc_image(latest_image,launch_configuration):
    if launch_configuration['ImageId'] != latest_image:
        needs_update = True
        logging.info("[compare_lc_image] Image update needed. Existing image: %s - Latest Image: %s" % (launch_configuration['ImageId'], latest_image))
    else:
        needs_update = False
        logging.info("[compare_lc_image] Image update not needed")
    return needs_update

def copy_lc(launch_configuration,autoscaling_group_name,image_id):

    t = datetime.now()
    try:
        new_name =  '%s-%s' % (autoscaling_group_name, t.strftime('%Y%m%d%H%M%S'))
        r = auto.create_launch_configuration(
        LaunchConfigurationName = new_name,
        AssociatePublicIpAddress = launch_configuration['AssociatePublicIpAddress'],
        BlockDeviceMappings = launch_configuration['BlockDeviceMappings'],
        EbsOptimized = launch_configuration['EbsOptimized'],
        IamInstanceProfile = launch_configuration['IamInstanceProfile'],
        ImageId = image_id,
        InstanceMonitoring = launch_configuration['InstanceMonitoring'],
        InstanceType = launch_configuration['InstanceType'],
        KeyName = launch_configuration['KeyName'],
        SecurityGroups = launch_configuration['SecurityGroups'],
        UserData = base64.decodestring(launch_configuration['UserData'])
    )
        if r['ResponseMetadata']['HTTPStatusCode'] >= 200 and r['ResponseMetadata']['HTTPStatusCode'] < 300:
            logging.info('[copy_lc] Launch Configuration successfully created')
            return new_name
        else:
            logging.exception('[copy_lc] Launch configuration creation failed')
            return False

    except Exception as e:
        logging.exception('[copy_lc] %s' % e)

def assign_lc(new_launch_configuration,autoscaling_group_name):
    try:
        auto.update_auto_scaling_group(AutoScalingGroupName=autoscaling_group_name, LaunchConfigurationName=new_launch_configuration)
        logging.info('[assign_lc] Launch configuration successfully applied to: %s'% autoscaling_group_name)
    except Exception as e:
        logging.exception('[assign_lc] %s' % e)

if __name__ == '__main__':
	main()
