# Update AWS CoreOS Image

This script polls CoreOS to determine if a new AMI has been released and will update the target AWS Autoscaling Group if found to be true. This coupled with an auto-updating policy with the existing CoreOS nodes in your environment will ensure extra reboots/updates are not executed when CoreOS releases a new image. The executor of this script either needs IAM privileges or an IAM role ARN needs to be provided.



**Arguments**

`-a`, `--autoscalinggroup` - The name of the autoscaling group image that will be compared to the latest AMI from CoreOS

`-r`, `--role-arn` - The ARN of the role to assume if you are going across accounts